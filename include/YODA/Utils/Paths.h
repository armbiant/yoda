// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2021 The YODA collaboration (see AUTHORS for details)
//
#include <string>

namespace YODA {


  /// Get the path to the directory containing libYODA
  std::string libPath();

  /// Get the path to the installed share/YODA/ data directory
  std::string dataPath();


}
