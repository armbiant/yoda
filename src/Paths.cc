// -*- C++ -*-
//
// This file is part of YODA -- Yet more Objects for Data Analysis
// Copyright (C) 2008-2021 The YODA collaboration (see AUTHORS for details)
//
#include "YODA/Utils/Paths.h"
#include "binreloc/binreloc.h"

using namespace std;

namespace YODA {


  string libPath() {
    BrInitError error;
    br_init_lib(&error);
    char* temp = br_find_lib_dir(DEFAULTLIBDIR);
    const string libdir(temp);
    free(temp);
    return libdir;
  }

  string dataPath() {
    BrInitError error;
    br_init_lib(&error);
    char* temp = br_find_data_dir(DEFAULTDATADIR);
    const string sharedir(temp);
    free(temp);
    return sharedir + "/YODA";
  }


}
